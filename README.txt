Emily Cox (ecox4)
Side Scroller App
CS 441 Project 4 Readme
___________________________

Sources include:
http://stackoverflow.com/questions/21703194/endless-scrolling-background-in-spritekit
	=>used for scrolling background setup and help with spritekit usage(still not 100%)
https://developer.apple.com/library/content/documentation/GraphicsAnimation/Conceptual/SpriteKit_PG/GettingStarted/GettingStarted.html
https://developer.apple.com/reference/spritekit?language=objc
	=>both used for help setting up and understanding spritekit
https://developer.apple.com/library/content/samplecode/SpriteKit_Physics_Collisions/Listings/SpriteKit_Physics_Collisions_APLSpaceScene_m.html#//apple_ref/doc/uid/DTS40013390-SpriteKit_Physics_Collisions_APLSpaceScene_m-DontLinkElementID_9
	=>used for understanding and implementing collisions

Issues:
	Collision has issues and the background does not loop as desired.