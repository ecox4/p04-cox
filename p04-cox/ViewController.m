//
//  ViewController.m
//  p04-cox
//
//  Created by Em on 3/2/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "ViewController.h"
#import "InitScene.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SKView *spriteView = (SKView *)self.view;
    spriteView.showsDrawCount = YES;
    spriteView.showsFPS = YES;
    spriteView.showsNodeCount = YES;
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated{
    InitScene* init = [[InitScene alloc] initWithSize:CGSizeMake(800, 1200)];
    SKView *spriteView = (SKView *)self.view;
    [spriteView presentScene: init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
