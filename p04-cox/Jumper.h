//
//  Jumper.h
//  p04-cox
//
//  Created by Em on 3/2/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Jumper : UIView
@property (nonatomic) float dx, dy;  // Velocity
@end

