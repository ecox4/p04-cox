//
//  GameScene.m
//  p04-cox
//
//  Created by Em on 3/17/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "GameScene.h"
#import "InitScene.h"
#include <time.h>
#include<stdlib.h>
#include<stdio.h>

@interface GameScene()
@property BOOL contentCreated;
@property BOOL gameOver;
@property int hits;
@property int speedJump;
@end

@implementation GameScene{
    SKSpriteNode *runner;
    SKSpriteNode *bg;
    SKSpriteNode *hurdle;
    CGPoint jumpSpeed;
}

-(void)didMoveToView:(SKView *)view{
    if(!self.contentCreated){
        [self createSceneContents];
        self.contentCreated = YES;
        self.gameOver = NO;
        self.hits = 0;
        self.backgroundColor = [SKColor whiteColor];
        jumpSpeed.y = 100;
        jumpSpeed.x = 0;
    }
}
-(SKSpriteNode *)newHurdle{
    hurdle = [SKSpriteNode spriteNodeWithImageNamed:@"brick.jpg"];
    hurdle.physicsBody.categoryBitMask = hurdleCat;
    hurdle.physicsBody.usesPreciseCollisionDetection = YES;
    hurdle.position = CGPointMake(800,50);
    hurdle.name = @"hurdle";
    return hurdle;
}
-(SKSpriteNode *)setScrollBack{
    bg = [SKSpriteNode spriteNodeWithImageNamed:@"trees.png"];
    bg.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame)+150);
    //make scroll
    bg.name = @"back";
    return bg;
}
-(SKSpriteNode *)setStaticBack{
    SKSpriteNode *bgNode = [SKSpriteNode spriteNodeWithImageNamed:@"sky.jpg"];
    bgNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    bgNode.name = @"staticBg";
    return bgNode;
}
-(SKSpriteNode *)setRunner{
    runner = [SKSpriteNode spriteNodeWithImageNamed:@"saltsprite_1.png"];
    runner.position = CGPointMake(CGRectGetMidX(self.frame)-100,50);
    runner.physicsBody.affectedByGravity = YES;
    runner.physicsBody.categoryBitMask = runCat;
    runner.physicsBody.usesPreciseCollisionDetection = YES;
    runner.name = @"runner";
    return runner;
}
-(void)moveBg{
    [bg runAction:[SKAction repeatActionForever:[SKAction moveByX:-50 y:0 duration: 2.0]]];
    //how to tile?
    return;
}
-(void)moveWall{
    [hurdle runAction:[SKAction repeatActionForever:[SKAction moveByX:-50 y:0 duration: 2.0]]];
    return;
}
-(void)update:(NSTimeInterval)currentTime{
    srand((unsigned int)time(NULL));
    self.speedJump = (int)(rand()%500)+300;
    jumpSpeed.y = self.speedJump;
    CGPoint pos = hurdle.position;
    if(hurdle.position.x < 1){
        pos.x = self.frame.size.width + 5;
    }
    hurdle.position = pos;
}
-(void)didBeginContact:(SKPhysicsContact *)contact{
    SKPhysicsBody *player;
    SKPhysicsBody *wall;
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
        player = contact.bodyB;
        wall = contact.bodyA;
    }
    self.hits++;
}
-(void)createSceneContents{
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody.categoryBitMask = floorCat;
    [self addChild:[self setStaticBack]];
    [self addChild:[self setScrollBack]];
    [self moveBg];
    [self addChild:[self setRunner]];
    [self addChild:[self newHurdle]];
    [self moveWall];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if(self.gameOver){
        SKNode *hNode = [self childNodeWithName:@"hurdle"];
        SKNode *rNode = [self childNodeWithName:@"runner"];
        SKNode *tNode = [self childNodeWithName:@"back"];
        SKNode *sNode = [self childNodeWithName:@"staticBg"];
        if(sNode != nil){
            hNode.name =nil;
            sNode.name = nil;
            tNode.name = nil;
            rNode.name = nil;
            SKAction *remove = [SKAction removeFromParent];
            [hNode runAction:remove];
            [rNode runAction:remove];
            [sNode runAction:remove];
            [tNode runAction:remove];
            [sNode runAction:remove completion:^{
                SKScene *init = [[InitScene alloc] initWithSize:self.size];
                SKTransition *tr = [SKTransition crossFadeWithDuration:0.5];
                [self.view presentScene:init transition:tr];
            }];
        }
    }else{
        if(self.hits == 2){
            self.gameOver = YES;
        }
    }
    CGPoint position = runner.position;
    position.y += jumpSpeed.y;
    runner.position = position;
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    CGPoint position = runner.position;
    position.y = 50;
    runner.position = position;
}
@end
