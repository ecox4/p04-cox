//
//  InitScene.m
//  p04-cox
//
//  Created by Em on 3/13/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "InitScene.h"
#import "GameScene.h"

@interface InitScene()
@property BOOL contentCreated;
@end

@implementation InitScene

-(void)didMoveToView: (SKView *)view{
    if(!self.contentCreated){
        [self createSceneContents];
        self.contentCreated = YES;
    }
}
-(void)createSceneContents{
    self.backgroundColor = [SKColor blackColor];
    self.scaleMode = SKSceneScaleModeAspectFill;
    [self addChild:[self newInitNode]];
    [self addChild:[self newSaltNode]];
}

-(SKSpriteNode *)newSaltNode{
    SKSpriteNode *saltNode = [SKSpriteNode spriteNodeWithImageNamed:@"saltsprite_2.png"];
    saltNode.position = CGPointMake(CGRectGetMidX(self.frame), 300);
    saltNode.name = @"saltNode";
    return saltNode;
}
-(SKLabelNode *)newInitNode{
    SKLabelNode *initNode = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    initNode.text = @"begin";
    initNode.fontSize = 38;
    initNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
    initNode.name = @"initNode";
    return initNode;
}

-(void)touchesBegan:(NSSet *) touches withEvent:(UIEvent *)event{
    SKNode *initNode = [self childNodeWithName:@"initNode"];
    SKNode *saltNode = [self childNodeWithName:@"saltNode"];
    if(initNode != nil){
        initNode.name = nil;
        saltNode.name = nil;
        SKAction *zoom = [SKAction scaleTo:2.0 duration:0.25];
        SKAction *pause = [SKAction waitForDuration:0.5];
        SKAction *fadeAway = [SKAction fadeOutWithDuration:0.25];
        SKAction *remove = [SKAction removeFromParent];
        SKAction *moveSequence = [SKAction sequence:@[zoom,pause,fadeAway,remove]];
        [initNode runAction:moveSequence];
        [saltNode runAction:remove];
        
        [initNode runAction:moveSequence completion:^{
            SKScene *game = [[GameScene alloc] initWithSize:self.size];
            SKTransition *tr = [SKTransition crossFadeWithDuration:0.5];
            [self.view presentScene:game transition:tr];
        }];
    }
}
@end
